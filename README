Yocto Project Documentation
===========================

This repository contains documentation materials for a variety of the Yocto
Project components. In particular see the README file under documentation/.

An online version of the documentation can be found at:
    https://docs.yoctoproject.org

Contributing
============

Please refer to our contributor guide here: https://docs.yoctoproject.org/contributor-guide/
for full details on how to submit changes.

As a quick guide, patches should be sent to docs@lists.yoctoproject.org
The git command to do that would be:

     git send-email -M -1 --to docs@lists.yoctoproject.org

The 'To' header can be set as default for this repository:

     git config sendemail.to docs@lists.yoctoproject.org

Now you can just do 'git send-email origin/master..' to send all local patches.

Read the documentation/README and documentation/standards.md files
for rules to follow when contributing to the documentation.

Git repository: https://git.yoctoproject.org/yocto-docs
Mailing list: docs@lists.yoctoproject.org
